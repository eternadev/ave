package com.eternadev.ave;

import java.util.*;

/**
 * Template for objects of type {@link Universe};
 * Describes environment on which the algorithm should be tested.
 * Created by eternadev.com on 20/11/15.
 */
public abstract class Universe extends Thread implements Observer, Reportable {

    private Map<Location, List<Entity>> entities = new HashMap<>();
    private static boolean run = true;

    public Universe() {}

    @Override
    public void run() {
        init();
        while(run) {
            nextIteration();
        }
    }

    public abstract void init();

    public abstract void nextIteration();

    public void finish() {
        if(run) run = false;
    }

    public boolean addEntity(Entity entity) {
        List<Entity> localEntities = entities.get(entity.getLocation());
        if(null == localEntities) {
            localEntities = new ArrayList<>();
            entities.put(entity.getLocation(), localEntities);
        }
        return localEntities.add(entity);
    }

    public boolean removeEntity(Entity entity) {
        List<Entity> localEntities = entities.get(entity.getLocation());
        if(null == localEntities) {
            return false;
        }
        return localEntities.remove(entity);
    }

    public Map<Location, List<Entity>> getEntities() {
        return entities;
    }

    public long countEntities() {
        long count = 0;
        for(Location location : entities.keySet()) {
            count += entities.get(location).size();
        }
        return count;
    }

    public long countEntities(Class<?> entityType) {
        long count = 0;
        for(Location location : entities.keySet()) {
            for(Entity entity : entities.get(location)) {
                if(entity.getClass().equals(entityType)) count++;
            }
        }
        return count;
    }

    public long countLocations() {
        return entities.size();
    }
}

















