package com.eternadev.ave;

/**
 * Describes the ability of com.eternadev.ave.Entity to act.
 * Created by eternadev.com on 26.11.15.
 * @link com.eternadev.ave.Entity
 */
public interface Actable {

    Reportable act(Situation situation);

}
