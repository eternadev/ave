package com.eternadev.ave;

/**
 * Created by anton on 28.11.15.
 */
public interface Relocatable {

    /**
     * Describes the logic of relocation.
     * @param newLocation New {@link Location} of {@link Entity}
     * @return Report about relocation of type Reportable.
     */
    Reportable relocate(Location newLocation);

}
