package com.eternadev.ave;

import java.util.List;

/**
 * Template for Action implementation. Defines data process logic.
 * Created by eternadev.com on 22/05/15.
 */
public abstract class Action<T> {

    private List<T> values;

    public Action(List<T> values) {
        this.values = values;
    }

    public abstract Result calculateResult();

    public List<T> getValues() {
        return values;
    }
}
