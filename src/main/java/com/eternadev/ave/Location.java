package com.eternadev.ave;

/**
 * Basic Location object.
 * Created by eternadev.com on 20/05/15.
 */
public class Location<T> {

    private final T[] coordinate;

    /**
     * Initializes new Location with provided coordinate.
     * @param coordinate Generic type of data
     */
    public Location(T[] coordinate){
        this.coordinate = coordinate;
    }

    public T[] getCoordinate() {
        return this.coordinate;
    }

    /**
     * Checks equality of two Locations by comparing coordinates.
     * @param other Object of type Location
     * @return true if coordinates are equal, otherwise false
     */
    @Override
    public boolean equals(Object other) {
        Location otherLocation = (Location)other;
        boolean equals = true;
        if(this.coordinate.length == otherLocation.getCoordinate().length) {
            for(int dimension = 0; dimension < coordinate.length; dimension++) {
                if(this.coordinate[dimension] != otherLocation.getCoordinate()[dimension]) {
                    equals = false;
                    break;
                }
            }
        } else {
            equals = false;
        }
        return equals;
    }
}
