package com.eternadev.ave;

/**
 * Utility for object instantiation.
 * Created by eternadev.com on 30.11.15.
 */
public abstract class ObjectInstantiation {

    /**
     * Instantiates object by package-qualified class name (like "java.lang.Integer").
     * @param className Classname of object to be created
     * @return Instantiated object
     */
    public static Object create(String className) {
        Object object;
        try {
            object = Class.forName(className).newInstance();
        } catch (ClassNotFoundException cnfe) {
            throw new RuntimeException(cnfe);
        } catch (InstantiationException ie) {
            throw new RuntimeException(ie);
        } catch (IllegalAccessException iae) {
            throw new RuntimeException(iae);
        }
        return object;
    }
}
