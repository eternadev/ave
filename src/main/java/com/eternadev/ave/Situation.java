package com.eternadev.ave;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Template for Situation object, which describes Entities around given Location.
 * Created by eternadev.com on 24/05/15.
 * {@link Entity} {@link Location}
 */
public abstract class Situation extends ArrayList<Entity> {

    private final Location center;

    /**
     * @param center Of type {@link Location} - defines center of situation;
     * @param surrounding Collection of type {@link Entity}, describing situation around center;
     */
    public Situation(Location center, Entity... surrounding) {
        super();
        Collections.addAll(this, surrounding);
        this.center = center;
    }

    /**
     * Returns center of described situation of type {@link Location}
     * @return center of described situation
     */
    public Location getCenter() {
        return center;
    }
}
