package com.eternadev.ave;

import java.util.Observable;

/**
 * Basic Entity object.
 * Created by eternadev.com on 21/05/15.
 * {@link Location} {@link Observable}
 */
public abstract class Entity extends Observable {

    private Location location;

    public Entity() {}

    public Entity(Location location) {
        this.location = location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return this.location;
    }

    /**
     * @return true if this Entity implements {@link Actable} interface
     */
    public boolean isActable() {
        return this instanceof Actable;
    }

    /**
     * @return true if this Entity implements {@link Relocatable} interface
     */
    public boolean isRelocatable() {
        return this instanceof Relocatable;
    }
}
