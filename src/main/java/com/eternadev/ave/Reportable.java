package com.eternadev.ave;

/**
 * Interface for various types of reports
 * Created by eternadev.com on 21/05/15.
 */
public interface Reportable<T> {

    T getReport();

}
