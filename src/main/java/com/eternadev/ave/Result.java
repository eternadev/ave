package com.eternadev.ave;

/**
 * Gives a wrap to calculations results
 * Created by eternadev.com on 23/05/15.
 */
public class Result<V> {

    private V value;

    public Result(V value) {
        this.value = value;
    }

    public V getValue() { return this.value; }
}
