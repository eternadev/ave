import com.eternadev.ave.*;

/**
 * Test entity
 * Created by eternadev.com on 28.11.15.
 */
public class RelocatableEntity extends Entity implements Relocatable {

    public RelocatableEntity() {super();}

    public RelocatableEntity(Location location) {
        super(location);
    }

    @Override
    public Reportable relocate(Location newLocation) {
        return null;
    }
}
