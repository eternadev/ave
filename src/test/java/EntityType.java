import com.eternadev.ave.Entity;
import com.eternadev.ave.Location;
import com.eternadev.ave.ObjectInstantiation;

/**
 * Describes types of possible entities and acts as a fabric for described entities;
 * ClassName value must match the actual class name otherwise returns NULL.
 * Created by eternadev.com on 26.11.15.
 */
public enum EntityType {

    DEFAULT_ENTITY("DefaultEntity"),
    ACTABLE_ENTITY("ActableEntity"),
    RELOCATABLE_ENTITY("RelocatableEntity");

    private String className;

    EntityType(String className) {
        this.className = className;
    }

    public Entity create(Location location) throws RuntimeException {
        Package p = this.getClass().getPackage();

        String packageName = null != p ? this.getClass().getPackage().getName() + "." : "";
        Entity entity;
        entity = (Entity)ObjectInstantiation.create(packageName + className);
        if(null != entity) entity.setLocation(location);
        return entity;
    }

    public static Entity create(EntityType entityType, Location location) {
        return entityType.create(location);
    }

    public static Entity create(String className, Location location) {
        return getType(className).create(location);
    }

    public static EntityType getType(String className) {
        for(EntityType entityType : values()) {
            if(entityType.getClassName().equals(className)) return entityType;
        }
        return null;
    }

    public String getClassName() {
        return this.className;
    }
}
