import com.eternadev.ave.*;

/**
 * Test entity
 * Created by eternadev.com on 28.11.15.
 */
public class ActableEntity extends Entity implements Actable {

    public ActableEntity() {super();}

    public ActableEntity(Location location) {
        super(location);
    }

    @Override
    public Reportable act(Situation situation) {
        return null;
    }

}
