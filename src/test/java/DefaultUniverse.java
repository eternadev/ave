import com.eternadev.ave.Universe;

import java.util.Observable;

/**
 * Test universe
 * Created by eternadev.com on 02.12.15.
 */
public class DefaultUniverse extends Universe {

    @Override
    public void init() {

    }

    @Override
    public void nextIteration() {

    }

    @Override
    public void update(Observable o, Object arg) {

    }

    @Override
    public Object getReport() {
        return null;
    }
}
