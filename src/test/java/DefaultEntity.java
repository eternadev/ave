import com.eternadev.ave.Entity;
import com.eternadev.ave.Location;

/**
 * Default implementation of {@link Entity} template.
 * Created by eternadev.com on 27.11.15.
 */
public class DefaultEntity extends Entity {

    public DefaultEntity() {super();}

    public DefaultEntity(Location location) {
        super(location);
    }

}
