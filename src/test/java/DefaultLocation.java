import com.eternadev.ave.Location;

/**
 * Basic implementation of com.eternadev.ave.Location template.
 * @link com.eternadev.ave.Location
 * Created by eternadev.com on 27.11.15.
 */
public class DefaultLocation<T> extends Location {

    /**
     * Initializes new com.eternadev.ave.Location with provided coordinate.
     *
     * @param coordinate Normalized data as coordinate of type long[]
     */
    public DefaultLocation(T[] coordinate) {
        super(coordinate);
    }
}
