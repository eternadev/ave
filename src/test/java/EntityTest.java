import com.eternadev.ave.Entity;
import org.junit.Assert;
import org.junit.Test;

/**
 * Entity tests
 * Created by eternadev.com on 28.11.15.
 */
public class EntityTest {

    @Test
    public void isActable() {
        Entity entity = new ActableEntity(new DefaultLocation(new Long[0]));
        Assert.assertTrue(entity.isActable());
    }

    @Test
    public void isRelocatable() {
        Entity entity = new RelocatableEntity(new DefaultLocation(new Long[0]));
        Assert.assertTrue(entity.isRelocatable());
    }

    @Test
    public void isDefault() {
        Entity entity = EntityType.DEFAULT_ENTITY.create(new DefaultLocation(new Long[0]));
        Assert.assertTrue(!entity.isActable() && !entity.isRelocatable());
    }
}
