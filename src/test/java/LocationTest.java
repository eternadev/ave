import com.eternadev.ave.Location;
import org.junit.Assert;
import org.junit.Test;

/**
 * Location test
 * Created by eternadev.com on 27.11.15.
 */
public class LocationTest {

    @Test
    public void equals() {
        Location one = new DefaultLocation(new Long[]{0L});
        Location two = new DefaultLocation(new Long[]{0L});
        Assert.assertTrue(one.equals(two));
        two = new DefaultLocation(new Long[]{1L});
        Assert.assertFalse(one.equals(two));
    }
}
