import com.eternadev.ave.Action;
import com.eternadev.ave.Result;

import java.util.List;

/**
 * Sample implementation of {@link Action} for testing purpose.
 * Created by eternadev.com on 01.12.15.
 */
public class Add extends Action {

    public Add(List<?> values) {
        super(values);
    }

    @Override
    public Result calculateResult() {
        Integer result = 0;
        for(Integer value : (List<Integer>)getValues()) {
            result += value;
        }
        return new Result(result);
    }
}
