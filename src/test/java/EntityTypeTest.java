import com.eternadev.ave.Location;
import org.junit.Assert;
import org.junit.Test;

/**
 * Various entity types tests
 * Created by eternadev.com on 27.11.15.
 */
public class EntityTypeTest {

    @Test
    public void build() {
        Location location = new DefaultLocation(new Long[0]);
        boolean result = true;
        try {
            for(EntityType type : EntityType.values()) {
                type.create(location);
            }
        } catch(RuntimeException e) {
            System.err.println(e);
            result = false;
        }
        Assert.assertTrue(result);
    }
}
