import com.eternadev.ave.Entity;
import com.eternadev.ave.Location;
import com.eternadev.ave.Universe;
import org.junit.Assert;
import org.junit.Test;

/**
 * Universe tests
 * Created by eternadev.com on 02.12.15.
 */
public class UniverseTest {

    @Test
    public void addEntity() {
        Location location = new DefaultLocation(new Long[0]);
        Entity entity = new DefaultEntity(location);
        Universe universe = new DefaultUniverse();
        universe.addEntity(entity);
        Assert.assertTrue(universe.getEntities().get(location).size() == 1);
    }

    @Test
    public void countEntities() {
        Location location = new DefaultLocation(new Long[0]);
        Entity entity = new RelocatableEntity(location);
        Universe universe = new DefaultUniverse();
        universe.addEntity(entity);
        Assert.assertTrue(universe.countEntities() == 1);
        Assert.assertTrue(universe.countEntities(RelocatableEntity.class) == 1);
        Assert.assertFalse(universe.countEntities(DefaultEntity.class) == 1);
    }

    @Test
    public void removeEntity() {
        Location location = new DefaultLocation(new Long[0]);
        Entity entity = new DefaultEntity(location);
        Universe universe = new DefaultUniverse();
        boolean result;
        if(universe.addEntity(entity)) {
            result = universe.removeEntity(entity);
        } else {
            result = false;
        }
        Assert.assertTrue(result);
    }

    @Test
    public void run() {
        Universe universe = new DefaultUniverse();
        universe.start();
        Assert.assertTrue(universe.isAlive());
    }

    @Test
    public void finish() {
        Universe universe = new DefaultUniverse();
        universe.start();
        boolean result;
        if(universe.isAlive()) {
            universe.finish();
            long millis = System.currentTimeMillis();
            while(System.currentTimeMillis() - millis < 100);
            result = !universe.isAlive();
        } else {
            result = false;
        }
        Assert.assertTrue(result);
    }
}
