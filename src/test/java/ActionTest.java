import com.eternadev.ave.Action;
import com.eternadev.ave.Result;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Action test
 * Created by eternadev.com on 01.12.15.
 */
public class ActionTest {

    @Test
    public void calculateResult() {
        List<Integer> values = new ArrayList<>();
        Integer expectedResult = 0;
        for(int value = 1; value < 5; value++) {
            values.add(value);
            expectedResult += value;
        }

        Action add = new Add(values);
        Result<Integer> result = add.calculateResult();
        Assert.assertEquals(expectedResult, result.getValue());
    }
}
